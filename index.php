<?php
require_once ('animal.php');
require_once ('Frog.php');
require_once ('ape.php');

$sheep = new Animals('Shaun');
echo "Name : ". $sheep->name . "<br>";
echo "Legs : ". $sheep->legs . "<br>";
echo "Cold blooded : ". $sheep->cold_blooded . "<br><br>";

$kudok = new Frog('buduk');
echo "Name : ". $kudok->name . "<br>";
echo "Legs : ". $kudok->legs . "<br>";
echo "Cold blooded : ". $kudok->cold_blooded . "<br>";
echo $kudok->Jump("Hop Hop <br><br>");

$sungokong = new Ape ('Kera sakti');
echo "Name : ". $sungokong->name . "<br>";
echo "Legs : ". $sungokong->legs . "<br>";
echo "Cold blooded : ". $sungokong->cold_blooded . "<br>";
echo $sungokong->Yell("Auooo")

?>